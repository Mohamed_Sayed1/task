﻿using MVCTASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTASK.ViewModels
{
    public class UserCart
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<Products> ShoppingCarts { get; set; }
        public decimal total { get; set; }
    }
}