﻿using MVCTask.ViewModels;
using MVCTASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTASK.Controllers
{
    public class AccountController : Controller
    {
        MVCTASKDataEntities context = new MVCTASKDataEntities();
        // GET: AccountMVCTaskDataBaseEntities1
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
           
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try {

                    User user = new User { name = model.Name, email = model.Email, password = model.Password };
                    context.User.Add(user);
                    context.SaveChanges();
                    return RedirectToAction("Index", "Products",new {id=user.id });
                }
                catch
                {
                    return View(model);
                }
              
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (model != null)
                {
                    var user = context.User.Where(u => u.password == model.Password && u.email==model.Email).FirstOrDefault();
                    if (user!=null){
                        return RedirectToAction("Index", "Products", new { id = user.id });

                    }
                }
               
            }
            return View(model);


        }

    }
}