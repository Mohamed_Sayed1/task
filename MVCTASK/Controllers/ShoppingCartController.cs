﻿using MVCTASK.Models;
using MVCTASK.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVCTASK.Controllers
{
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        private MVCTASKDataEntities db = new MVCTASKDataEntities();

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult DisplayeShoppingCart(int id)
        {
            ViewBag.id = id;
            var user = db.User.Find(id);
            decimal total = 0;
            var cart = new UserCart();
            var ProductList = from s in db.ShoppingCarts
                              join p in db.Products on s.ProductID equals p.id
                              where s.UserId == id
                              select p;

            cart.ShoppingCarts = ProductList.ToList();
            
            foreach (var item in ProductList)
            {
                total = total + item.Price;
            }
            cart.total = total;
            cart.name = user.name;
            return View(cart);
        }
      
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShoppingCarts cart = db.ShoppingCarts.Where(s=>s.ProductID==id).FirstOrDefault();
            if (cart == null)
            {
                return HttpNotFound();
            }
            db.ShoppingCarts.Remove(cart);
            db.SaveChanges();
            return RedirectToAction("DisplayeShoppingCart", new { id = id });
        }


    }
}