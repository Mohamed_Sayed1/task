﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTASK.Models;
using MVCTASK.ViewModels;

namespace MVCTASK.Controllers
{
    public class ProductsController : Controller
    {
        int id;
        private MVCTASKDataEntities db = new MVCTASKDataEntities();

        // GET: Products
        public ActionResult Index(int id)
        {
            ViewBag.id = id;
            TempData["id"]=id;
            TempData.Keep("id");
            this.id = id;
            return View(db.Products.ToList());
        }
        public ActionResult Add(int ProductId,int id)
        {
            ShoppingCarts cart = new ShoppingCarts();
            cart.UserId = id;
            cart.ProductID = ProductId;
            db.ShoppingCarts.Add(cart);
           int z= db.SaveChanges();
            return RedirectToAction("Index", new { id = id });
        }
        public ActionResult Search(SearchItem SearchItem)
        {
            ViewBag.id = SearchItem. id;
            IQueryable<Products> Products;
            Products = from e in db.Products
                        select e;
            if (!String.IsNullOrEmpty(SearchItem.searchString))
            {
                int result;
                int.TryParse(SearchItem.searchString, out result);
                if (result > 0)
                {
                    Products = from e in db.Products
                               where e.Price == result || e.Quantity == result.ToString()
                               select e;


                }
                else
                {
                    Products = from e in db.Products
                               where e.Name== SearchItem.searchString
                               select e;
                }


            }
            return View("Index",  Products);

        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // GET: Products/Create
        public ActionResult Create(int id)
        {
            ViewBag.id = id;
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Name,Price,Quantity,ImgPath")] Products products, HttpPostedFileBase file,int id)
        {
          
            try
            {
               
                if (ModelState.IsValid)
                {
                if (file != null && file.ContentLength > 0)
                {  

                        string path = Path.Combine(Server.MapPath("~/Images"),
                                                   Path.GetFileName(file.FileName));
                        file.SaveAs(path);
                        products.ImgPath = file.FileName;
                        db.Products.Add(products);
                        db.SaveChanges();
                        return RedirectToAction("Index",new {id= id } );
                    }
                }
        }
                catch (Exception ex)
                {
                return View(products);
            }

            return View(products);



        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.id = TempData["id"];
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Name,Price,Quantity,ImgPath")] Products products)
        {
            if (ModelState.IsValid)
            {
                db.Entry(products).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = TempData["id"] });
            }
            return View(products);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Products products = db.Products.Find(id);
            db.Products.Remove(products);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = TempData["id"] });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
